package id.co.nexsoft.form.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.form.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
    List<Province> findAll();
}
