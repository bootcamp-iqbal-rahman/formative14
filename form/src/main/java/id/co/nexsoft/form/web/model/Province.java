package id.co.nexsoft.form.web.model;

public class Province {
    private int id;
    private String name;
    private Country country_id;

    public Province() {}

    public Province(String name, Country country) {
        this.name = name;
        this.country_id = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country_id;
    }

    public void setCountry(Country country_id) {
        this.country_id = country_id;
    }
}
