package id.co.nexsoft.form.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.form.model.City;

public interface CityRepository extends JpaRepository<City, Integer> {
    
}
