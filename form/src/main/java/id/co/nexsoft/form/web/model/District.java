package id.co.nexsoft.form.web.model;

public class District {
    private int id;
    private String name;
    private City city_id;

    public District() {}

    public District(String name, City city) {
        this.name = name;
        this.city_id = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city_id;
    }

    public void setCity(City city_id) {
        this.city_id = city_id;
    }
}
