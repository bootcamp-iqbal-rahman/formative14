package id.co.nexsoft.form.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.form.model.Country;
import id.co.nexsoft.form.repository.CountryRepository;
import id.co.nexsoft.form.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService<Country> {
    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<Country> getAllData() {
        return countryRepository.findAll();
    }

    @Override
    public Country getDataById(int id) {
        return countryRepository.findById(id).get();
    }

    @Override
    public Country saveData(Country data) {
        return countryRepository.save(data);
    }

    @Override
    public ResponseEntity<Country> updateData(Country data, int id) {
        Optional<Country> countryOptional = countryRepository.findById(id);

        if (!countryOptional.isPresent())
            return ResponseEntity.notFound().build();

        data.setId(id);
        countryRepository.save(data);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteData(int id) {
        countryRepository.deleteById(id);
    }
}
