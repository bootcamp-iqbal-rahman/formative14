package id.co.nexsoft.form.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.form.model.District;
import id.co.nexsoft.form.service.CountryService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
@RequestMapping(path = "/district")
public class DistrictController {
    @Autowired
    private CountryService<District> countryService;

    @GetMapping
    public List<District> getAllData() {
        return countryService.getAllData();
    }

    @GetMapping("/{id}")
    public District getDataById(@PathVariable int id) {
        return countryService.getDataById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public District createData(@RequestBody District district) {
        return countryService.saveData(district);
    }

    @PutMapping("/{id}")
    public ResponseEntity<District> updateData(
            @RequestBody District data,
            @PathVariable int id) {
        return countryService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        countryService.deleteData(id);
    }
}
