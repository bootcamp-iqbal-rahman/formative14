package id.co.nexsoft.form.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.form.model.District;

public interface DistrictRepository extends CrudRepository<District, Integer> {
    List<District> findAll();
}
