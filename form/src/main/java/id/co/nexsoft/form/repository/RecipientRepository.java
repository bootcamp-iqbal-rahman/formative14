package id.co.nexsoft.form.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.form.model.Recipient;

public interface RecipientRepository extends JpaRepository<Recipient, Integer> {
    List<Recipient> findAll();
}
