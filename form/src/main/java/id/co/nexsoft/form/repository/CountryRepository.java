package id.co.nexsoft.form.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.form.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
    List<Country> findAll();
}
