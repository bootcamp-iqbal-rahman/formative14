package id.co.nexsoft.form.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface CountryService<T> {
    List<T> getAllData();
    T getDataById(int id);
    T saveData(T data);
    ResponseEntity<T> updateData(T data, int id);
    void deleteData(int id);
}
