package id.co.nexsoft.form.web.controller;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.nexsoft.form.web.model.District;
import id.co.nexsoft.form.web.model.Recipient;

@Controller
@SessionAttributes("name")
@RequestMapping("/form")
public class FormController {

    @GetMapping
    public String showFormPage(ModelMap model) {
        return "form";
    }

    @PostMapping
    public String showThankYouPage(@RequestParam("name") String name,
                                @RequestParam("gender") String gender,
                                @RequestParam("street") String street,
                                @RequestParam("zipcode") int zipcode,
                                @RequestParam(value = "district") int district,
                                @RequestParam(value = "assurance", required = false, defaultValue = "off") String assurance,
                                @RequestParam(value = "desc", required = false, defaultValue = "") String desc,
                                ModelMap model) {
        String apiUrl = "http://localhost:8080/recipient";
        District districts = new District();
        districts.setId(district);
        Recipient recipient = new Recipient(name, gender, street, zipcode, assurance, desc, districts);
        
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writeValueAsString(recipient);

            HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .uri(URI.create(apiUrl))
                .header("Content-Type", "application/json")
                .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
            model.put("message", "Thank you for filling the form ... " + response.statusCode());
        } catch (Exception e) {
            model.put("message", "Error processing your data");
        }

        return "index";
    }
    
}
