package id.co.nexsoft.form.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.form.model.Province;
import id.co.nexsoft.form.service.CountryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping(path = "/province")
public class ProvinceController {
    @Autowired
    private CountryService<Province> provinceService;

    @GetMapping
    public List<Province> getAllData() {
        return provinceService.getAllData();
    }

    @GetMapping("/{id}")
    public Province getDataById(@PathVariable int id) {
        return provinceService.getDataById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Province createData(@RequestBody Province data) {
        return provinceService.saveData(data);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Province> updateData(
            @RequestBody Province data,
            @PathVariable int id) {
        return provinceService.updateData(data, id);
    }
}
