package id.co.nexsoft.form.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.form.model.District;
import id.co.nexsoft.form.repository.DistrictRepository;
import id.co.nexsoft.form.service.CountryService;

@Service
public class DistrictServiceImpl implements CountryService<District> {
    @Autowired
    private DistrictRepository districtRepository;

    @Override
    public List<District> getAllData() {
        return districtRepository.findAll();
    }

    @Override
    public District getDataById(int id) {
        return districtRepository.findById(id).get();
    }

    @Override
    public District saveData(District data) {
        return districtRepository.save(data);
    }

    @Override
    public ResponseEntity<District> updateData(District data, int id) {
        Optional<District> districtOptional = districtRepository.findById(id);

        if (!districtOptional.isPresent())
            return ResponseEntity.notFound().build();

        data.setId(id);
        districtRepository.save(data);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteData(int id) {
        districtRepository.deleteById(id);
    }
}
