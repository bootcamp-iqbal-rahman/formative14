package id.co.nexsoft.form.web.controller;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.nexsoft.form.web.model.Recipient;

@Controller
@RequestMapping("/data")
public class DetailController {

    @GetMapping
    public String showDetailPage(ModelMap model) {
        String apiUrl = "http://localhost:8080/recipient";
        String[][] data;

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(apiUrl))
                .GET()
                .build();

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            JsonNode jsonNode = objectMapper.readTree(httpResponse.body());
            List<Recipient> people = new ArrayList<>();
            for (JsonNode personNode : jsonNode) {
                Recipient person = objectMapper.treeToValue(personNode, Recipient.class);
                people.add(person);
            }
            data = new String[people.size()][10];
            int id = 0;
            for (Recipient p : people) {
                data[id][0] = p.getName();
                data[id][1] = p.getGender();
                data[id][2] = p.getStreet();
                data[id][3] = String.valueOf(p.getZip_code());
                data[id][4] = p.getDistrict().getName();
                data[id][5] = p.getDistrict().getCity().getName();
                data[id][6] = p.getDistrict().getCity().getProvince().getName();
                data[id][7] = p.getDistrict().getCity().getProvince().getCountry().getName();
                data[id][8] = p.getAssurance();
                data[id][9] = p.getDescription();
                id += 1;
            }

            model.put("data", data);
        } catch (Exception e) {
            model.put("message", "Error getting data list ... : ");
        }

        return "detail";
    }
}
