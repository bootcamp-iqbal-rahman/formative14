<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <title>Invoice Form</title>
    <link rel="stylesheet" href="https://codepen.io/gymratpacks/pen/VKzBEp#0" >
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,300" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="/css/style.css" >
</head>

<body>
    <div class="row">
        <div class="col-md-12"><h1>${message}</h1></div>
        <div class="col-md-12"> 
            <c:forEach var="d" items="${data}">
                <form>
                    <h1>Sending Address</h1>

                    <fieldset>
                        <label for="name">Recipient:</label>
                        <input type="text" id="name" name="name" value="${d[0]}" disabled>

                        <p>Gender:</p>
                        <c:choose>
                            <c:when test="${d[1] == 'male'}">
                                <input type="radio" id="male" value="male" name="gender" checked disabled>
                                <label for="male" class="light">Male</label><br>
                                <input type="radio" id="female" value="female" name="gender" disabled>
                                <label for="female" class="light">Female</label><br><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" id="male" value="male" name="gender" disabled>
                                <label for="male" class="light">Male</label><br>
                                <input type="radio" id="female" value="female" name="gender" checked disabled>
                                <label for="female" class="light">Female</label><br><br>
                            </c:otherwise>
                        </c:choose>

                        <label for="street">Street:</label>
                        <input type="text" id="street" name="street" value="${d[2]}" disabled>

                        <label for="zipcode">Zip Code:</label>
                        <input type="text" id="zipcode" name="zipcode" value="${d[3]}" disabled>

                        <label for="district">District:</label>
                        <input type="text" id="district" name="district" value="${d[4]}" disabled>

                        <label for="city">City:</label>
                        <input type="text" id="city" name="city" value="${d[5]}" disabled>

                        <label for="province">Province:</label>
                        <input type="text" id="province" name="province" value="${d[6]}" disabled>

                        <label for="country">Country:</label>
                        <select id="country" name="country" value="${d[7]}" disabled>
                            <optgroup label="Asia">
                                <option value="indonesia">Indonesian</option>
                                <option value="singapura">Singapura</option>
                                <option value="malaysia">Malaysia</option>
                                <option value="australia">Australia</option>
                            </optgroup>
                        </select>

                        <label>Assurance? (10%):</label>
                        <c:choose>
                            <c:when test="${d[8] == 'on'}">
                                <input type="checkbox" id="assurance" value="assurance" name="assurance" checked>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" id="assurance" value="assurance" name="assurance">
                            </c:otherwise>
                        </c:choose>
                        <label class="light" for="assurance">Checkbox</label><br >

                        <textarea id="desc" name="desc">${d[9]}</textarea>

                        <label for="price">Total Price:</label>
                        <input type="text" id="price" name="price" placeholder="Rp 1.250.000" disabled>

                        <label for="delivery">Delivery Service Cost:</label>
                        <input type="text" id="delivery" name="delivery" placeholder="Rp 50.000" disabled>

                    </fieldset>
                </form>
            </c:forEach>
        </div>
    </div>
</body>

</html>