<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <title>Invoice Form</title>
    <link rel="stylesheet" href="https://codepen.io/gymratpacks/pen/VKzBEp#0" >
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,300" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="/css/style.css" >
</head>

<body>
    <div class="row">
        <div class="col-md-12"> 
            <form action="/form" method="post">
                <h1>Sending Address</h1>

                <fieldset>
                    <label for="name">Recipient:</label>
                    <input type="text" id="name" name="name" placeholder="Your name" required>

                    <p>Gender:</p>
                    <input type="radio" id="male" value="male" name="gender">
                    <label for="male" class="light">Male</label><br>
                    <input type="radio" id="female" value="female" name="gender">
                    <label for="female" class="light">Female</label><br><br>

                    <label for="street">Street:</label>
                    <input type="text" id="street" name="street" placeholder="Street" required>

                    <label for="zipcode">Zip Code:</label>
                    <input type="text" id="zipcode" name="zipcode" placeholder="Zip code" required>

                    <label for="district">District:</label>
                    <input type="text" id="district" name="district" placeholder="District" required>

                    <label for="city">City:</label>
                    <input type="text" id="city" name="city" placeholder="City" required>

                    <label for="province">Province:</label>
                    <input type="text" id="province" name="province" placeholder="Province" required>

                    <label for="country">Country:</label>
                    <select id="country" name="country" required>
                        <optgroup label="Asia">
                            <option value="indonesia">Indonesian</option>
                            <option value="singapura">Singapura</option>
                            <option value="malaysia">Malaysia</option>
                            <option value="australia">Australia</option>
                        </optgroup>
                    </select>

                    <label>Assurance? (10%):</label>
                    <input type="checkbox" id="assurance" value="assurance" name="assurance" >
                    <label class="light" for="assurance">Checkbox</label><br >

                    <textarea id="desc" name="desc"></textarea>

                    <label for="price">Total Price:</label>
                    <input type="text" id="price" name="price" placeholder="Rp 1.250.000" disabled>

                    <label for="delivery">Delivery Service Cost:</label>
                    <input type="text" id="delivery" name="delivery" placeholder="Rp 50.000" disabled>

                </fieldset>
                <button type="submit">Send</button>
            </form>
        </div>
    </div>
</body>

</html>