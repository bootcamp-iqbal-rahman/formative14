<%@ page contentType="text/html; charset=Shift_JIS" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <title>Invoice Form</title>
    <link rel="stylesheet" href="https://codepen.io/gymratpacks/pen/VKzBEp#0" >
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,300" rel="stylesheet" type="text/css" >
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css" >
</head>

<body>
    <div class="row">
        <h1>${message}</h1>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <button><a href="/form">Fill Form</a></button>
        </div>
        <div class="col-md-3"> 
            <button><a href="/data">See Data</a></button>
        </div>
        <div class="col-md-3"></div>
    </div>
</body>

</html>