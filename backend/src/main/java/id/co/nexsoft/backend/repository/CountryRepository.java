package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.backend.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
    List<Country> findAll();
}
