package id.co.nexsoft.backend.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.Recipient;
import id.co.nexsoft.backend.repository.RecipientRepository;
import id.co.nexsoft.backend.service.CountryService;

@Service
public class RecipientServiceImpl implements CountryService<Recipient> {
    @Autowired
    private RecipientRepository recipientRepository;

    @Override
    public List<Recipient> getAllData() {
        return recipientRepository.findAll();
    }

    @Override
    public Recipient getDataById(int id) {
        return recipientRepository.findById(id).get();
    }

    @Override
    public Recipient saveData(Recipient data) {
        return recipientRepository.save(data);
    }

    @Override
    public ResponseEntity<Recipient> updateData(Recipient data, int id) {
        Optional<Recipient> recipientOptional = recipientRepository.findById(id);

        if (!recipientOptional.isPresent())
            return ResponseEntity.notFound().build();

        data.setId(id);
        recipientRepository.save(data);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteData(int id) {
        recipientRepository.deleteById(id);
    }
}
