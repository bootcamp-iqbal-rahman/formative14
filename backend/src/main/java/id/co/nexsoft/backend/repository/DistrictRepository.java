package id.co.nexsoft.backend.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.backend.model.District;

public interface DistrictRepository extends CrudRepository<District, Integer> {
    List<District> findAll();
}
