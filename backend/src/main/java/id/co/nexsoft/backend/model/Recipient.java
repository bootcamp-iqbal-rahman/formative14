package id.co.nexsoft.backend.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "recipient")
public class Recipient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "street")
    private String street;

    @Column(name = "zip_code")
    private int zip_code;

    @Column(name = "assurance")
    private String assurance;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district_id;

    public Recipient() {}

    public Recipient(int id, String name, String gender, String street, 
                    int zip_code, String assurance, String description,
                    District district) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.street = street;
        this.zip_code = zip_code;
        this.assurance = assurance;
        this.description = description;
        this.district_id = district;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZip_code() {
        return zip_code;
    }

    public void setZip_code(int zip_code) {
        this.zip_code = zip_code;
    }

    public String getAssurance() {
        return assurance;
    }

    public void setAssurance(String assurance) {
        this.assurance = assurance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public District getDistrict() {
        return district_id;
    }

    public void setDistrict(District district_id) {
        this.district_id = district_id;
    }
}
