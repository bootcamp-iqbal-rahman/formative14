package id.co.nexsoft.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.City;

public interface CityRepository extends JpaRepository<City, Integer> {
    
}
