package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
    List<Province> findAll();
}
