package id.co.nexsoft.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.backend.model.City;
import id.co.nexsoft.backend.service.CountryService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
@RequestMapping(path = "/city")
public class CityController {
    @Autowired
    private CountryService<City> countryService;

    @GetMapping
    public List<City> getAllData() {
        return countryService.getAllData();
    }

    @GetMapping("/{id}")
    public City getDataById(@PathVariable int id) {
        return countryService.getDataById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public City createData(@RequestBody City city) {
        return countryService.saveData(city);
    }

    @PutMapping("/{id}")
    public ResponseEntity<City> updateData(
            @RequestBody City data,
            @PathVariable int id) {
        return countryService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        countryService.deleteData(id);
    }
}
