package id.co.nexsoft.backend.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.Province;
import id.co.nexsoft.backend.repository.ProvinceRepository;
import id.co.nexsoft.backend.service.CountryService;

@Service
public class ProvinceServiceImpl implements CountryService<Province> {

    @Autowired
    private ProvinceRepository provinceRepository;

    @Override
    public List<Province> getAllData() {
        return provinceRepository.findAll();
    }

    @Override
    public Province getDataById(int id) {
        return provinceRepository.findById(id).get();
    }

    @Override
    public Province saveData(Province data) {
        return provinceRepository.save(data);
    }

    @Override
    public ResponseEntity<Province> updateData(Province data, int id) {
        Optional<Province> provinceOptional = provinceRepository.findById(id);

        if (!provinceOptional.isPresent())
            return ResponseEntity.notFound().build();

        data.setId(id);
        provinceRepository.save(data);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteData(int id) {
        provinceRepository.deleteById(id);
    }
    
}
