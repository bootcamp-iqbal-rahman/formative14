package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.Recipient;

public interface RecipientRepository extends JpaRepository<Recipient, Integer> {
    List<Recipient> findAll();
}
