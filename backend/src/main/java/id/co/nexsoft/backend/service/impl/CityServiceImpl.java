package id.co.nexsoft.backend.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.City;
import id.co.nexsoft.backend.repository.CityRepository;
import id.co.nexsoft.backend.service.CountryService;

@Service
public class CityServiceImpl implements CountryService<City> {
    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<City> getAllData() {
        return cityRepository.findAll();
    }

    @Override
    public City getDataById(int id) {
        return cityRepository.findById(id).get();
    }

    @Override
    public City saveData(City data) {
        return cityRepository.save(data);
    }

    @Override
    public ResponseEntity<City> updateData(City data, int id) {
        Optional<City> cityOptional = cityRepository.findById(id);

        if (!cityOptional.isPresent())
            return ResponseEntity.notFound().build();

        data.setId(id);
        cityRepository.save(data);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteData(int id) {
        cityRepository.deleteById(id);
    }
}
